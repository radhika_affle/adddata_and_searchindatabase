package com.example.radhikasharma.datafromfiletodatabase;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by radhikasharma on 30/10/17.
 */

public class DataBaseItems {


    public int prod_id;
    public int subp_id;
    public String prod_code;
    public String prod_desc;
    public String subpr_code;
    public String subpr_desc;
    public String shade_name;
    public String shade_code;



    public DataBaseItems() {
    }



    public DataBaseItems(int prod_id,int subp_id,String prod_code,String prod_desc,String subpr_code,String subpr_desc,String shade_name,String shade_code) {
        this.prod_id=prod_id;
        this.subp_id=subp_id;
        this.prod_code=prod_code;
        this.prod_desc=prod_desc;
        this.subpr_code=subpr_code;
        this.subpr_desc=subpr_desc;
        this.shade_name=shade_name;
        this.shade_code=shade_code;
    }

    public int getProd_id()
    {
        return prod_id;
    }
    public int getsubp_id(){ return subp_id;}
    public String getProd_code(){return  prod_code;}
    public String getProd_desc() {return prod_desc;}
    public String getSubpr_code(){return  subpr_code;}
    public String getSubpr_desc(){return subpr_desc;}
    public String getShade_name(){return  shade_name;}
    public String getShade_code(){return shade_code;}

    public void setProd_id(int prod_id){this.prod_id=prod_id;}
    public void setSubp_id(int subp_id){this.subp_id=subp_id;}
    public void setProd_code(String prod_code){this.prod_code=prod_code;}
    public void setProd_desc(String prod_desc){this.prod_desc=prod_desc;}
    public void setSubpr_code(String subpr_code){this.subpr_code=subpr_code;}
    public void setSubpr_desc(String subpr_desc){this.subpr_desc=subpr_desc;}
    public void setShade_name(String shade_name){this.shade_name=shade_name;}
    public void setShade_code(String shade_code){this.shade_code=shade_code;}

}



