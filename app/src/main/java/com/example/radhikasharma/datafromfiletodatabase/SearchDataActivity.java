package com.example.radhikasharma.datafromfiletodatabase;

import android.app.ProgressDialog;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;

import java.util.List;


public class SearchDataActivity extends AppCompatActivity implements InterfaceCallBack {

    private RecyclerView recyclerView;
    private AdapterClass mAdapter;
    DataBaseHandler db;
    List<DataBaseItems> data;
    String TAG="hey";
    ProgressDialog progress;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search_data);
        recyclerView = (RecyclerView) findViewById(R.id.recyclerView);
        recyclerView.setHasFixedSize(true);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(SearchDataActivity.this);
        linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        db=new DataBaseHandler(this);
        data=db.getAllContacts();
        mAdapter = new AdapterClass(SearchDataActivity.this, data,SearchDataActivity.this);
        recyclerView.setAdapter(mAdapter);
        recyclerView.setLayoutManager(linearLayoutManager);
        Log.v(TAG, "recyclerview ");
        progress=new ProgressDialog(SearchDataActivity.this);
    }


    public boolean onCreateOptionsMenu(Menu menu) {
        Log.v(TAG,"menu entered");

        getMenuInflater().inflate(R.menu.menu_search_file, menu);

        MenuItem search = menu.findItem(R.id.search);
        SearchView searchView = (SearchView) MenuItemCompat.getActionView(search);

        search(searchView);
        return true;
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        return super.onOptionsItemSelected(item);
    }

    private void search(SearchView searchView) {

        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
               progress.setMessage("SEARCHING");

                progress.setCancelable(false);
                progress.show();

                mAdapter.getFilter().filter(query);

                return true;

            }

            @Override
            public boolean onQueryTextChange(String newText) {

                return true;
            }
        });
    }

    @Override
    public void closeDialog() {
        Log.v(TAG,"inside close dialog");
        progress.dismiss();
    }
}



