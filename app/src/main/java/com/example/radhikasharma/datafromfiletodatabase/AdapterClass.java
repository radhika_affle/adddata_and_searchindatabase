package com.example.radhikasharma.datafromfiletodatabase;

import android.app.ProgressDialog;
import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ProgressBar;
import android.widget.TextView;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by radhikasharma on 31/10/17.
 */

public class AdapterClass extends  RecyclerView.Adapter<RecyclerView.ViewHolder>  {

    private Context context;
    private LayoutInflater inflater;
    List<DataBaseItems> data;
    DataBaseItems current;
    InterfaceCallBack interfaceCallBack;
    String TAG = "AdapterClass";


    public AdapterClass(Context context, List<DataBaseItems> data,InterfaceCallBack interfaceCallBack) {
        this.context = context;
        inflater = LayoutInflater.from(context);
        this.data = data;
        this.interfaceCallBack=interfaceCallBack;

    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
       View     view = inflater.inflate(R.layout.card_view, parent, false);
        MyHolder holder = new MyHolder(view);


        return holder;
    }


    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {

        MyHolder myHolder = (MyHolder) holder;
        current = data.get(position);
        myHolder.prod_id.setText(String.valueOf(current.prod_id));
        myHolder.subpr_id.setText(String.valueOf(current.subp_id));
        myHolder.prod_code.setText((current.prod_code));
        myHolder.prod_desc.setText(String.valueOf(current.prod_desc));
        myHolder.subpr_code.setText(String.valueOf(current.subpr_code));
        myHolder.subpr_desc.setText(String.valueOf(current.subpr_desc));
        myHolder.shade_name.setText(String.valueOf(current.shade_name));
        myHolder.shade_code.setText(String.valueOf(current.shade_code));

        Log.v(TAG, "value obtained " + data.size());
    }

    @Override
    public int getItemCount() {
        Log.v(TAG, "size");
        return data.size();

    }

    public Filter getFilter() {

        return new Filter() {
            List<DataBaseItems> mFilteredList =new ArrayList<>();
            @Override
            protected FilterResults performFiltering(CharSequence charSequence) {
                Log.v(TAG,"perform filtering");
                String charString = charSequence.toString().toLowerCase();

                if (charSequence.toString().isEmpty()) {
                    Log.v(TAG,"empty list");
                    mFilteredList.addAll(data);
                } else {
                    Log.v(TAG,"list");
                    ArrayList<DataBaseItems> filteredList = new ArrayList<>();

                    Log.v(TAG,"List size "+data.size());

                    for (int i = 0; i < data.size(); i++) {

                        final DataBaseItems text = data.get(i);
                       Log.v(TAG,""+charSequence);
                          Log.v(TAG,"inside loop");
                        if(charString.matches(".*\\d.*")){
                            if(text.getProd_id() == Integer.parseInt(charString) ||  text.getsubp_id() == (Integer.parseInt(charString)) || text.getShade_code().equalsIgnoreCase(charString))
                            {
                                filteredList.add(data.get(i));
                            }
                        } else{

                        if (  text.getProd_code().equalsIgnoreCase(charSequence.toString()) || text.getProd_desc().equalsIgnoreCase(charSequence.toString()) || text.getSubpr_code().equalsIgnoreCase(charSequence.toString()) || text.getSubpr_desc().equalsIgnoreCase(charSequence.toString()) || text.getShade_name().toLowerCase().contains(charSequence.toString()) || text.getSubpr_code().toLowerCase().contains(charSequence.toString())) {
                            filteredList.add(data.get(i));
                            Log.v(TAG, "" + filteredList);
                        } }
                    }
                    Log.v(TAG,"filter method");

                    mFilteredList.addAll(filteredList);
                }

                FilterResults filterResults = new FilterResults();
                filterResults.values = mFilteredList;
                return filterResults;
            }

            @Override
            protected void publishResults(CharSequence charSequence, FilterResults filterResults) {
                mFilteredList = (ArrayList<DataBaseItems>) filterResults.values;
                Log.v(TAG,"values updated");
                if(mFilteredList.size() == 0) {
                  Log.v(TAG,"enetered 0 ");
                    mFilteredList.addAll(data);
                    data.clear();
                    data.addAll(mFilteredList);
                    notifyDataSetChanged();
                    interfaceCallBack.closeDialog();
                }
                else{
                    Log.v(TAG,"hello data");
                    data.clear();
                    data.addAll(mFilteredList);
                    notifyDataSetChanged();
                    interfaceCallBack.closeDialog();

                }}
        };
    }
}

    class MyHolder extends RecyclerView.ViewHolder {

        TextView prod_id;
        TextView subpr_id;
        TextView prod_code;
        TextView prod_desc;
        TextView subpr_code;
        TextView subpr_desc;
        TextView shade_name;
        TextView shade_code;

        public MyHolder(View itemView) {
            super(itemView);
            prod_id = (TextView) itemView.findViewById(R.id.text1);
            subpr_id = (TextView) itemView.findViewById(R.id.text2);
            prod_code = (TextView) itemView.findViewById(R.id.text3);
            prod_desc = (TextView) itemView.findViewById(R.id.text4);
            subpr_code = (TextView) itemView.findViewById(R.id.text5);
            subpr_desc = (TextView) itemView.findViewById(R.id.text6);
            shade_name = (TextView) itemView.findViewById(R.id.text7);
            shade_code = (TextView) itemView.findViewById(R.id.text8);

        }

    }



