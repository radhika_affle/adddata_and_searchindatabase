package com.example.radhikasharma.datafromfiletodatabase;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by radhikasharma on 30/10/17.
 */


public class DataBaseHandler extends SQLiteOpenHelper {

    private static final int DATABASE_VERSION = 1;
    private static final String DATABASE_NAME = "Products";
    private static final String TABLE_CONTACTS = "ProductInfo";
    public static final String TAG="activity";
    // Contacts Table Columns names
    private static final String KEY_PROD_ID= "prod_id";
    private static final String KEY_SUBP_ID = "subp_id";
    private static final String KEY_PROD_CODE = "prod_code";
    private static final String KEY_PROD_DESC= "prod_desc";
    private static final String KEY_SUBP_CODE = "subp_code";
    private static final String KEY_SUBP_DESC = "subp_desc";
    private static final String KEY_SHADE_NAME = "shade_name";
    private static final String KEY_SHADE_CODE = "shade_code";
    int countt=0;

    public DataBaseHandler(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    // Creating Tables
    @Override
    public void onCreate(SQLiteDatabase db) {
        String CREATE_CONTACTS_TABLE = "CREATE TABLE " + TABLE_CONTACTS + "("
                + KEY_PROD_ID + " INTEGER," + KEY_SUBP_ID + " INTEGER,"
                + KEY_PROD_CODE + " TEXT," + KEY_PROD_DESC + "  TEXT," + KEY_SUBP_CODE + " TEXT,"+ KEY_SUBP_DESC + " TEXT,"
                +KEY_SHADE_NAME + " TEXT,"+ KEY_SHADE_CODE + " TEXT"+ ")";
        db.execSQL(CREATE_CONTACTS_TABLE);
    }

    // Upgrading database
    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_CONTACTS);
        onCreate(db);
    }

    void addData(DataBaseItems data) {
        SQLiteDatabase db = this.getWritableDatabase();
         db.beginTransaction();

        try{
            {
                ContentValues values = new ContentValues();

                values.put(KEY_PROD_ID, data.getProd_id());
                values.put(KEY_SUBP_ID, data.getsubp_id());
                values.put(KEY_PROD_CODE, data.getProd_code());
                values.put(KEY_PROD_DESC, data.getProd_desc());
                values.put(KEY_SUBP_CODE, data.getSubpr_code());
                values.put(KEY_SUBP_DESC, data.getSubpr_desc());
                values.put(KEY_SHADE_NAME, data.getShade_name());
                values.put(KEY_SHADE_CODE, data.getShade_code());

                db.insert(TABLE_CONTACTS, null, values);
            } Log.v(TAG,"hello");
        countt++;
        Log.v(TAG,"value"+countt);
            db.setTransactionSuccessful();
     //   db.close();

    }catch(Exception e){
    e.printStackTrace();}
        finally{
            db.endTransaction();
        }
    }


    public List<DataBaseItems> getAllContacts() {
        List<DataBaseItems> contactList = new ArrayList<DataBaseItems>();
        // Select All Query
        String selectQuery = "SELECT  * FROM " + TABLE_CONTACTS;

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {

                DataBaseItems contact = new DataBaseItems();
                contact.setProd_id(Integer.parseInt(cursor.getString(0)));
                contact.setSubp_id(Integer.parseInt(cursor.getString(1)));
                contact.setProd_code(cursor.getString(2));
                contact.setProd_desc(cursor.getString(3));
                contact.setSubpr_code(cursor.getString(4));
                contact.setSubpr_desc(cursor.getString(5));
                contact.setShade_name(cursor.getString(6));
                contact.setShade_code(cursor.getString(7));
                // Adding contact to list
                contactList.add(contact);
            } while (cursor.moveToNext());
        }

        // return contact list
        return contactList;
    }}
