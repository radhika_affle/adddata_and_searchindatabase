package com.example.radhikasharma.datafromfiletodatabase;

import android.app.ProgressDialog;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonToken;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.io.InputStream;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    Button onClick,text;
    DataBaseHandler db;
    final String TAG = "activity";
    List<DataBaseItems> contacts;
    ProgressDialog progressbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        onClick = (Button) findViewById(R.id.onclick);
        text = (Button) findViewById(R.id.viewDatabase);
        db = new DataBaseHandler(this);
        progressbar=new ProgressDialog(MainActivity.this);
        onClick.setOnClickListener(new Button.OnClickListener() {

            @Override
            public void onClick(View arg0) {
                try {
                   Log.v(TAG,"inside");
                   readFileFromAsset("Idex.json");

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
        text.setOnClickListener(new Button.OnClickListener() {

            @Override
            public void onClick(View arg0) {
                try {
                    Log.v(TAG,"inside");
                    viewfromDatabase();

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });

    }


    public void readFileFromAsset(String filename) {
        try {
            Log.v(TAG, "inside method");
            progressbar.setMessage("SEARCHING");

            progressbar.setCancelable(false);
            progressbar.show();

            InputStream is = getAssets().open(filename);
            ObjectMapper mapper = new ObjectMapper();
            JsonParser jp = mapper.getFactory().createParser(is);
            JsonToken current;
            current = jp.nextToken();
            if (current != JsonToken.START_OBJECT) {
                System.out.println("Error: root should be object: quiting.");
                return;
            }
            while (jp.nextToken() != JsonToken.END_OBJECT) {
                String fieldName = jp.getCurrentName();

                current = jp.nextToken();
                System.out.println("NAme: " + fieldName);
                DataBaseItems list = new DataBaseItems();
                if (fieldName.equals("GDATA")) {
                    if (current == JsonToken.START_ARRAY) {

                        while (jp.nextToken() != JsonToken.END_ARRAY) {

                            JsonNode node = jp.readValueAsTree();
                            list.prod_id = node.get("PROD_ID").asInt();
                            list.subp_id = node.get("SUBP_ID").asInt();
                            list.prod_code = node.get("PROD_CODE").asText();
                            list.prod_desc = node.get("PROD_DESC").asText();
                            list.subpr_code = node.get("SUBPR_CODE").asText();
                            list.subpr_desc = node.get("SUBPR_DESC").asText();
                            list.shade_name = node.get("SHADE_NAME").asText();
                            list.shade_code = node.get("SHADE_CODE").asText();
                            db.addData(list);


                        }
                        Log.v(TAG,"finished");
                        progressbar.dismiss();
                    } else {
                        System.out.println("Error: records should be an array: skipping.");
                        jp.skipChildren();
                    }
                } else {
                    System.out.println("Unprocessed property: " + fieldName);
                    jp.skipChildren();
                }
            }


        }

         catch (Exception ex) {
            ex.printStackTrace();

        }
    }
    public void viewfromDatabase() {
         contacts = db.getAllContacts();
         int count=0;
        for (DataBaseItems cn : contacts) {
            String log = "Prod Id: " + cn.getProd_id() + " ,SUBD Id: " + cn.getsubp_id() + " ,PROD Code: " + cn.getProd_code() + " , PROD Desc : " + cn.getProd_desc() + ", SUBPR Code : " + cn.getSubpr_code() +
                    " ,SUBPR Desc : " + cn.getProd_desc() + " , SHADE Name :" + cn.getShade_name() + " , SHADE Code :" + cn.getSubpr_code();
            count++;
            System.out.println(log);
            System.out.println(""+count);
        }

       if(contacts!=null)
       {
         Intent intent=new Intent(this,SearchDataActivity.class);
          startActivity(intent);
       }
        }
    }




